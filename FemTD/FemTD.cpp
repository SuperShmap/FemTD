// FemTD.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "Renderer.h"
#include "InputHandler.h"
#include "Media.h"
#include "LightingSystem.h"

int main()
{
	Media media;
		
	Renderer rend;
	rend.Init();

	Level::GetInstance().Init();
	Interface::GetInstance().Init(StateFlags::LEVEL_CREATOR, WinHeight);
	InputHandler::GetInstance().Init();

	SDL_Event e;

	LightingSystem::GetInstance().Init();

	rend.LoadTextures(media);

	Interface::GetInstance().FillInterfaceCreator();


	while (true)
	{
		while (SDL_PollEvent(&e) != 0)
		{
			InputHandler::GetInstance().MouseEvents(&e);
		}


		LightingSystem::GetInstance().LightObject();
		rend.Render(Interface::GetInstance(), Level::GetInstance());
	}

    return 0;
}

