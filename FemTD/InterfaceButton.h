#pragma once

#include "Buttons.h"
#include "PhysComponent.h"
#include "SpriteComponent.h"

class InterfaceButton : public GameObject
{
public:

	InterfaceButton(GameObject* object, int x, int y, int w, int h, const MouseMode mouseMode) : mObject(object),
		mode(mouseMode)
	{
		SDL_Rect pictogram;
		pictogram.x = x - 0.9 * x;
		pictogram.y = y + 0.1 * y;
		pictogram.w = 0.9 * h;
		pictogram.h = 0.9 * h;

		SDL_Rect button;
		button.x = x;
		button.y = y;
		button.w = w;
		button.h = h;

		AddComponent(new ButtonPictogramComponent(object->GetComponent<SpriteComponent>()->mTexture, pictogram));
		AddComponent(new SpriteComponent(Media::buttonTexture.get(), button));

		SDL_Rect textField;
		//Later add text textures. I hope so...
	}

	virtual ~InterfaceButton() {};

	GameObject* mObject = nullptr;
	MouseMode mode;
};