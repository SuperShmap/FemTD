#pragma once

#include "PhysComponent.h"
#include "Level.h"
#include "Interface.h"
#include "Media.h"
#include "SpriteComponent.h"
#include "LightComponent.h"

class Renderer
{
public:
	Renderer();
	~Renderer();

	void Init();
	void Close();

	void SetGameViewport();
	void SetInterfaceViewport(Interface& intface);

	void LoadTexture(Texture* texture);
	void LoadTextures(Media& media);

	void GameRender(GameState& level);
	void Render(Interface& intface, GameState& level);

private:
	void Free();

	SDL_Window* mWindow = nullptr;
	SDL_Renderer* mRenderer = nullptr;
	SDL_Texture* mTexture = nullptr;

	Media mMedia;

	int mGameWindowWidth;
};

