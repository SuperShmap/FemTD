#include "stdafx.h"
#include "Media.h"

Media::Media()
{
}

Media::~Media()
{
}

std::shared_ptr<Texture> Media::freeTexture = std::make_shared<Texture>("Resources/freeTexture");
std::shared_ptr<Texture> Media::freeCellTexture = std::make_shared<Texture>("Resources/freeCell.png");
std::shared_ptr<Texture> Media::interfaceBackground = std::make_shared<Texture>("Resources/interfaceBackground.png");
std::shared_ptr<Texture> Media::highlighting = std::make_shared<Texture>("Resources/light.png");
std::shared_ptr<Texture> Media::buttonTexture = std::make_shared<Texture>("Resources/button.png");
std::shared_ptr<Texture> Media::pathCellTexture = std::make_shared<Texture>("Resources/pathCell.png");
std::shared_ptr<Texture> Media::waypoint = std::make_shared<Texture>("Resources/waypoint");