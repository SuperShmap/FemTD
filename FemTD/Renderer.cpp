#include "stdafx.h"
#include "Renderer.h"


Renderer::Renderer()
{

}

Renderer::~Renderer()
{
}


void Renderer::Init()
{
	//Initializate SDL
	if (SDL_Init(SDL_INIT_VIDEO) < 0)
		printf("Initialization error! %s", SDL_GetError());

	//Initializate different image formats loading
	int imgFlags = IMG_INIT_JPG;
	if (!(IMG_Init(imgFlags) & imgFlags))
		printf("Image initialization failed. %s", IMG_GetError());

	//Create window
	mWindow = SDL_CreateWindow("Dont forget to do something with this text", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, WinWidth, WinHeight, SDL_WINDOW_SHOWN);
	if (mWindow == nullptr)
		printf("Can't create window. %s", SDL_GetError());

	//Create renderer
	mRenderer = SDL_CreateRenderer(mWindow, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
	if (mRenderer == nullptr)
		printf("Error create renderer %s", SDL_GetError());
}

void Renderer::Close()
{
	SDL_DestroyWindow(mWindow);
	mWindow = nullptr;
	SDL_DestroyRenderer(mRenderer);
	mRenderer = nullptr;

	SDL_Quit();
}

void Renderer::SetGameViewport()
{
	SDL_Rect gameWindow;
	gameWindow.x = 0;
	gameWindow.y = 0;
	gameWindow.w = WinHeight; //Level is square anyway
	gameWindow.h = WinHeight;
	
	SDL_RenderSetViewport(mRenderer, &gameWindow);
}

void Renderer::SetInterfaceViewport(Interface& intface)
{
	SDL_RenderSetViewport(mRenderer, &intface.mPosition);

	SDL_Rect interfacePosition; //Because in new viewport you count coordinates from 0.0 again
	interfacePosition.x = 0; 
	interfacePosition.y = 0;
	interfacePosition.w = intface.mPosition.w;
	interfacePosition.h = intface.mPosition.h;

	SDL_RenderCopy(mRenderer, intface.mSprite.mTexture->texture, NULL, &interfacePosition);
	
}

void Renderer::LoadTexture(Texture* texture)
{
	Free();

	SDL_Texture* newTexture = nullptr;
	SDL_Surface* loadedSurface = IMG_Load(texture->path.c_str());
	if (loadedSurface == nullptr)
		printf("Error loading image %s SDL Error: %s", texture->path.c_str(), IMG_GetError());
	else
	{
		SDL_SetColorKey(loadedSurface, SDL_TRUE, SDL_MapRGB(loadedSurface->format, 0, 0, 0xFF));
		newTexture = SDL_CreateTextureFromSurface(mRenderer, loadedSurface);
	}

	if (newTexture == nullptr)
		printf("Error create texture from surface %s", SDL_GetError());

	texture->texture = newTexture;
}

void Renderer::LoadTextures(Media& media)
{
	LoadTexture(media.freeCellTexture.get());
	LoadTexture(media.interfaceBackground.get());
	LoadTexture(media.highlighting.get());
	LoadTexture(media.buttonTexture.get());
	LoadTexture(media.pathCellTexture.get());
}

void Renderer::GameRender(GameState& level)
{
	for (GameObject* renderObjects : level.GetObjects())
	{
		if (renderObjects->GetComponent<SpriteComponent>()->mClip.w == 0 || renderObjects->GetComponent<SpriteComponent>()->mClip.h == 0)
		{
			printf("Error in draw texture. Clip size is 0 or Clip is missing");
			return;
		}

		if (renderObjects->GetComponent<SpriteComponent>()->mTexture->texture == nullptr)
		{
			printf("Object dont have a texture");
			return;
		}

		//Draw sprites
		SDL_RenderCopy(mRenderer, renderObjects->GetComponent<SpriteComponent>()->mTexture->texture, NULL, &renderObjects->GetComponent<SpriteComponent>()->mClip);
		//Light objects
		if (renderObjects->GetComponent<LightComponent>() != nullptr)
			SDL_RenderCopy(mRenderer, renderObjects->GetComponent<LightComponent>()->mTexture->texture, NULL, &renderObjects->GetComponent<SpriteComponent>()->mClip);
		//Draw button pictograms. I suppose, only interface can consist buttons.
		if (typeid(Interface) == typeid(level))
			SDL_RenderCopy(mRenderer, renderObjects->GetComponent<ButtonPictogramComponent>()->mTexture->texture, NULL, &renderObjects->GetComponent<ButtonPictogramComponent>()->mClip);
	}
}

void Renderer::Render(Interface& intface, GameState& level)
{
	SDL_SetRenderDrawColor(mRenderer, 0x00, 0xFF, 0xFF, 0xFF);
	SDL_RenderClear(mRenderer);

	SetGameViewport();
	GameRender(level);

	SetInterfaceViewport(intface);
	GameRender(intface);

	SDL_RenderPresent(mRenderer);
}

void Renderer::Free()
{
	if (mTexture == nullptr)
		return;

	SDL_DestroyTexture(mTexture);
	mTexture = nullptr;
}