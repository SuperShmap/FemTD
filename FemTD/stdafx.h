// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//

#pragma once

#include "targetver.h"

#include <stdio.h>
#include <tchar.h>
#include <vector>
#include <unordered_map>
#include <functional>
#include <memory>

#include <SDL.h>
#include <SDL_image.h>

#include "Media.h"

#undef main

const int WinWidth = 1024;
const int WinHeight = 720;

const int LevelSize = 20;
const int CellSize = 40;


// TODO: reference additional headers your program requires here
