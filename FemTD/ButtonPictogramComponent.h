#pragma once

#include "stdafx.h"
#include "Component.h"

class ButtonPictogramComponent : public Component
{
public:
	ButtonPictogramComponent(Texture* texture, SDL_Rect clip) : mClip(clip),
		mTexture(texture)
	{	}

	SDL_Rect mClip;
	Texture* mTexture;
};