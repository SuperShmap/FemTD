#include "stdafx.h"
#include "Level.h"


Level::~Level()
{
	for (GameObject* obj : mObjects)
		delete obj;
}

Level& Level::GetInstance()
{
	static Level instance;
	return instance;
}

void Level::Init()
{
	for (int i = 0; i < LevelSize * CellSize; i += CellSize)
	{
		for (int j = 0; j < LevelSize * CellSize; j += CellSize)
		{
			mObjects.push_back(new FreeCellEnt(j, i));
		}
	}
}

void Level::LoadLevel()
{
	//Its temporary dummy for levels.
	//In further it should loads from file

	mObjects.push_back(new FreeCellEnt(10, 10));
	
}
