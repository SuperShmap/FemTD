#pragma once

#include "stdafx.h"
#include "GameObject.h"

class PathCell : public GameObject
{
public:

	PathCell() //It needs for interface buttons. As an entity, interface buttons holds their own textures
	{				//and clip for it. It needs only for pictogram
		AddComponent(new SpriteComponent(Media::pathCellTexture.get(), 0, 0, cellSize, cellSize));
	}

	PathCell(int x, int y)
	{
		AddComponent(new SpriteComponent(Media::pathCellTexture.get(), x, y, cellSize, cellSize));
	}



	virtual ~PathCell()
	{
		for (Component* comp : mComponents)
			delete comp;
	}

	const int cellSize = CellSize;
};