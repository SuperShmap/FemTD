#pragma once

#include "GameState.h"
#include "FreeCellEnt.h"
#include "PathCell.h"
#include "InterfaceButton.h"
#include "Level.h"

class Interface : public GameState
{
public:
	static Interface& GetInstance();
	void Init(StateFlags flag, int position);

	void CreateInterfaceWindow();
	void AddButton(InterfaceButton* button);

	void FillInterfaceCreator();
	void FillInterfaceGame();

	virtual ~Interface();

	SDL_Rect mPosition;
	Texture* mBackgroundTexture;
	SpriteComponent mSprite;

private:

	using FillInterface = std::function <void()>;
	std::unordered_map<StateFlags, FillInterface> mInterfaceFillers;
	std::vector<GameObject*> mInterfaceObjects; //all possible objects hold here

	const uint32_t mButtonHeight = 100;

	Interface() {}
	Interface(Interface&) {}
	Interface& operator = (Interface&) {}
};