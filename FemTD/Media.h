#pragma once
#include "stdafx.h"
#include "Texture.h"

static class Media
{
public:
	Media();

	static std::shared_ptr<Texture> freeTexture;
	static std::shared_ptr<Texture> freeCellTexture;
	static std::shared_ptr<Texture> interfaceBackground;
	static std::shared_ptr<Texture> highlighting;
	static std::shared_ptr<Texture> buttonTexture;
	static std::shared_ptr<Texture> pathCellTexture;
	static std::shared_ptr<Texture> waypoint;

	~Media();
};