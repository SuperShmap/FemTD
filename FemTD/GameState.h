#pragma once

#include "GameObject.h"

class GameState
{
public:
	GameState() { };
	
	std::vector<GameObject*>& GetObjects()
	{
		return mObjects;
	}

	virtual ~GameState() { };

protected:
	std::vector<GameObject*> mObjects;
	StateFlags mFlag;
};

