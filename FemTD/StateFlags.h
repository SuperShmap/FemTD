#pragma once

enum class StateFlags
{
	GAME,
	LEVEL_CREATOR
};

StateFlags currentState;

enum class MouseMode
{
	Empty,
	FreeCell,
	PathCell
};