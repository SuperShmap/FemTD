#include "stdafx.h"
#include "Interface.h"

	//Useless old strings. I want to decribe interesting way for evade switch-case and need this thing
	/*mInterfaceFillers.insert(std::make_pair(StateFlags::GAME, std::bind(&Interface::FillInterfaceGame, this)));
	mInterfaceFillers.insert(std::make_pair(StateFlags::LEVEL_CREATOR, std::bind(&Interface::FillInterfaceCreator, this)));*/

Interface& Interface::GetInstance()
{
	static Interface instance;
	return instance;
}

void Interface::Init(StateFlags flag, int position)
{
	mFlag = flag;

	mPosition.x = position;
	mPosition.y = 0;
	mPosition.w = WinWidth - position;
	mPosition.h = WinHeight;

	mBackgroundTexture = Media::interfaceBackground.get();
	mSprite.mTexture = mBackgroundTexture;
	mSprite.mClip = mPosition;
}

void Interface::CreateInterfaceWindow()
{
	//@todo add dependency of state flags
	
}

void Interface::AddButton(InterfaceButton* button)
{
	mObjects.push_back(button);
}

void Interface::FillInterfaceCreator()
{
//	for (GameObject* object : mInterfaceObjects)
//		delete object;
	for (GameObject* object : mObjects)
		delete object;

	//At first, create list of all objects represented in interface
	//In mInterfaceOvbjects I hold buttons for all possible objects (in curent state!)
/*	mInterfaceObjects.push_back(new FreeCellEnt);
	mInterfaceObjects.push_back(new PathCell);*/

	//Then add it for array drawing
//	for (int i = 0; i < mInterfaceObjects.size(); i++)

	AddButton(new InterfaceButton(new FreeCellEnt, 0, 0 * mButtonHeight, mPosition.w, mButtonHeight, MouseMode::FreeCell));
	AddButton(new InterfaceButton(new PathCell, 0, 1 * mButtonHeight, mPosition.w, mButtonHeight, MouseMode::PathCell));

}

void Interface::FillInterfaceGame()
{
	for (GameObject* object : mInterfaceObjects)
		delete object;

	/*mInterfaceObjects.push_back(new FreeCellEnt);
	mInterfaceObjects.push_back(new FreeCellEnt);
	mInterfaceObjects.push_back(new FreeCellEnt);

	for (int i = 0; i < mInterfaceObjects.size(); i++)
		AddButton(new InterfaceButton(mInterfaceObjects[i], 0, i * mButtonHeight, mPosition.w, mButtonHeight));*/
}

Interface::~Interface()
{
	for (GameObject* button : mObjects)
		delete button;

	for (GameObject* object : mInterfaceObjects)
		delete object;

	delete mBackgroundTexture;
}
