#pragma once

#include "PhysComponent.h"
#include "SpriteComponent.h"
#include "LightComponent.h"
#include "ButtonPictogramComponent.h"
#include "StateFlags.h"
//@todo: incapsulate all include components in ine .h file

class GameObject
{
public:
	GameObject() {}

	template<class T>
	T* GetComponent()
	{
		T* component;
		for (Component* i : mComponents)
		{
			component = dynamic_cast<T*> (i);
			if (component != nullptr)
				return component;
		}
		return nullptr;
	}

	void AddComponent(Component* component)
	{
		mComponents.push_back(component);
	}

	template <class T>
	void DeleteComponent()
	{
		for (auto i = mComponents.begin(); i != mComponents.end();)
		{
			if (dynamic_cast<T*> (*i) != nullptr)
			{
				delete *i;
				i = mComponents.erase(i);
			}
			else
				++i;
		}
	}

	virtual ~GameObject() {}

	std::list<Component*> mComponents;
};