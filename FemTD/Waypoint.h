#pragma once
#include "stdafx.h"
#include "GameObject.h"

class Waypoint : public GameObject
{
public:
	Waypoint() {}

	~Waypoint()
	{
		for (Component* comp : mComponents)
			delete comp;
	}

	Waypoint(int x, int y)
	{
		if (currentState == StateFlags::LEVEL_CREATOR)
			AddComponent(new SpriteComponent(Media::waypoint.get(), x - x % CellSize, y - y % CellSize, CellSize, CellSize));
		else
			AddComponent(new SpriteComponent(Media::freeTexture.get(), x - x % CellSize, y - y % CellSize, CellSize, CellSize));
	}

	int number;
};