#pragma once
#include "GameState.h"
#include "Interface.h"
#include "GameObject.h"
#include "Cursors.h"
#include "CellFactory.h"

class InputHandler
{
public:
	~InputHandler();

	static InputHandler& GetInstance();
	void Init();

	GameObject* GetPointedObject();
	int GetObjectNumber();
	void MouseEvents(SDL_Event* e);

	int mX;
	int mY;

	template <class T>
	void PlaceObject()
	{
			int x;
			int y;
			x = GetPointedObject()->GetComponent<SpriteComponent>()->mClip.x;
			y = GetPointedObject()->GetComponent<SpriteComponent>()->mClip.y;
			delete GetPointedObject();
			GameObject* temp = GetPointedObject();
			temp = new T(x, y);
	}

private:
	CellFactory* mFactory;
	GameObject* mCursorObject = nullptr;

	MouseMode mMouseMode;

	InputHandler() {};
	InputHandler(InputHandler &) {};
	InputHandler& operator = (InputHandler&) {};
};