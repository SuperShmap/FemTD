#include "stdafx.h"
#include "InputHandler.h"


InputHandler& InputHandler::GetInstance()
{
	static InputHandler instance;
	return instance;
}

void InputHandler::Init()
{
	mCursorObject = new GameObject;
	mFactory = new CellFactory;
}

GameObject* InputHandler::GetPointedObject()
{
	if (mX < Interface::GetInstance().mPosition.x) //if cursor over level field
	{
		int objectNumber = (mY / CellSize) * LevelSize + mX / CellSize;
		if ((objectNumber >= 0) && (objectNumber < Level::GetInstance().GetObjects().size()))
			return Level::GetInstance().GetObjects()[objectNumber];
	}
	else //if cursor over intrface field
	{
		int buttonNumber = mY / Interface::GetInstance().GetObjects()[0]->GetComponent<SpriteComponent>()->mClip.h;
		if ((buttonNumber >= 0) && (buttonNumber < Interface::GetInstance().GetObjects().size()))
			return Interface::GetInstance().GetObjects()[buttonNumber];
	}
	return nullptr;
}

int InputHandler::GetObjectNumber()
{
	if (mX < Interface::GetInstance().mPosition.x)
		return (mY / CellSize) * LevelSize + mX / CellSize;
	else
		return mY / Interface::GetInstance().GetObjects()[0]->GetComponent<SpriteComponent>()->mClip.h;
}

void InputHandler::MouseEvents(SDL_Event* e)
{
	if (e->type == SDL_MOUSEMOTION)
	{
		SDL_GetMouseState(&mX, &mY);
		printf("Mouse coord %d, %d \n", mX, mY);
	}

	if (e->type == SDL_MOUSEBUTTONDOWN)
	{
		//Creates one of cursors. Each cursor class uses CursorModes enum class
		//for determining objects type that should be created.
		InterfaceButton* button = dynamic_cast<InterfaceButton*> (GetPointedObject());
		if (button != nullptr) //if we pointed on interface
			mMouseMode = button->mode;
		else
		{
			switch (mMouseMode)
			{
			case(MouseMode::Empty) : break;
			case(MouseMode::FreeCell) :
				mFactory->PlaceCell<FreeCellEnt>();
				break;
			case(MouseMode::PathCell) :
				mFactory->PlaceCell<PathCell>();
				break;
			default: break;
			}
		}
	}
}

InputHandler::~InputHandler()
{
	delete mCursorObject;
	delete mFactory;
}