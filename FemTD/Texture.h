#pragma once
#include "stdafx.h"

class Texture
{
public:
	Texture(std::string Texturepath) : path(Texturepath)
	{}

	SDL_Texture* texture = nullptr;
	std::string path;
};