#pragma once

#include "stdafx.h"
#include "GameObject.h"

class FreeCellEnt : public GameObject
{
public:

	FreeCellEnt() //It needs for interface buttons. As an entity, interface buttons holds their own textures
	{				//and clip for it. It needs only for pictogram
		AddComponent(new SpriteComponent(Media::freeCellTexture.get(), 0, 0, cellSize, cellSize));
	}

	FreeCellEnt(int x, int y)
	{	
		AddComponent(new SpriteComponent(Media::freeCellTexture.get(), x, y, cellSize, cellSize));
	}



	virtual ~FreeCellEnt() 
	{
		for (Component* comp : mComponents)
			delete comp;
	}

	const int cellSize = CellSize;
};