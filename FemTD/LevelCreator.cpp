#include "stdafx.h"
#include "LevelCreator.h"


LevelCreator::LevelCreator()
{
	for (int i = 0; i < LevelSize * CellSize; i += CellSize)
	{
		for (int j = 0; j < LevelSize * CellSize; j += CellSize)
		{
			mObjects.push_back(new FreeCellEnt(j, i));
		}
	}
}


LevelCreator::~LevelCreator()
{
	for (GameObject* obj : mObjects)
		delete obj;
}
