#pragma once
#include "stdafx.h"
#include "Component.h"

class PhysComponent : public Component
{
public:
	PhysComponent() {};

	PhysComponent(int x, int y, int w, int h)
	{
		mClip.x = x;
		mClip.y = y;
		mClip.w = w;
		mClip.h = h;
	}

	PhysComponent(SDL_Rect& rect) : mClip(rect)
	{
	}

	virtual ~PhysComponent()
	{	}

	SDL_Rect mClip;

};

