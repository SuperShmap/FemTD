#pragma once

#include "stdafx.h"
#include "GameState.h"
#include "InputHandler.h"

class LightingSystem
{
public:
	static LightingSystem& GetInstance()
	{
		static LightingSystem instance;
		return instance;
	}

	void LightingSystem::Init()
	{
		mPrevObject = Level::GetInstance().GetObjects()[0];
	}

	void LightObject()
	{
		//Also system should take a gamestate, because it needs in main menu too.
		if (InputHandler::GetInstance().GetPointedObject() != nullptr)
		{
			if (mPrevObject != InputHandler::GetInstance().GetPointedObject())
			{
				mPrevObject->DeleteComponent<LightComponent>();
				InputHandler::GetInstance().GetPointedObject()->AddComponent(new LightComponent);
				mPrevObject = InputHandler::GetInstance().GetPointedObject();
			}
		}
	}

	~LightingSystem()
	{
		delete mPrevObject;
	}
			

private:
	GameObject* mPrevObject = nullptr;

	LightingSystem() {}
	LightingSystem(LightingSystem&) {}
	LightingSystem& operator = (LightingSystem&) {}
};