#pragma once
#include "stdafx.h"
#include "GameState.h"
#include "Interface.h"
#include "GameObject.h"
#include "Cursors.h"

class CellFactory
{
public:
	template <class T>
	void PlaceCell()
	{
		int x = InputHandler::GetInstance().GetPointedObject()->GetComponent<SpriteComponent>()->mClip.x;
		int y = InputHandler::GetInstance().GetPointedObject()->GetComponent<SpriteComponent>()->mClip.y;
		delete InputHandler::GetInstance().GetPointedObject();
		GameObject* temp = InputHandler::GetInstance().GetPointedObject();
		temp = new T(x, y);
	}
};