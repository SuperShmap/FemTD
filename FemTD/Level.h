#pragma once

#include "GameState.h"
#include "GameObject.h"
#include "FreeCellEnt.h"
#include "PathCell.h"

//It's a scene. Level holds all game objects in game
class Level : public GameState
{
public:
	virtual ~Level();
	void LoadLevel();

	static Level& GetInstance();
	void Init();

private:
	std::vector<GameObject*> mLevelObjects;

	Level() {}
};
