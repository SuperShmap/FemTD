#pragma once

#include "stdafx.h"
#include "Component.h"

class LightComponent : public Component
{
public:
	LightComponent()
	{
		mTexture = Media::highlighting.get();
	}

	Texture* mTexture;
};