#pragma once
#include "InputHandler.h"
#include "Level.h"

enum class Modes
{
	FreeCell,
	PathCell
};

class CursorMode
{
public:
	CursorMode(Level& level) : mLevel(level)
	{	}
	virtual ~CursorMode() {}
	virtual void Place(int x, int y, int number) = 0;
	static CursorMode* SetMode(Modes mode);

	Level& mLevel;
};

class FreeCellMode : public CursorMode
{
	void Place(int x, int y, int number) override
	{
		
	}
};