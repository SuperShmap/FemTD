#pragma once

#include "stdafx.h"
#include "Component.h"

class SpriteComponent : public Component
{
public:
	SpriteComponent() {}

	SpriteComponent(Texture* texture, SDL_Rect& clip) : mTexture(texture), mClip(clip)
	{	}

	SpriteComponent(Texture* texture, int x, int y, int w, int h) : mTexture(texture)
	{
		mClip.x = x;
		mClip.y = y;
		mClip.w = w;
		mClip.h = h;
	}
	
	Texture* mTexture;
	SDL_Rect mClip;
};